package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/valyala/fasthttp"
	"github.com/zerodha/fastglue"
)

type App struct {
	db *redis.Client
}

func (a *App) handleIndex(ctx *fastglue.Request) error {
	count, err := a.db.Incr(ctx.RequestCtx, "counter").Result()
	if err != nil {
		fmt.Fprintf(ctx.RequestCtx, "error while incrementing counter: %v", err)
		return nil
	}

	fmt.Fprintf(ctx.RequestCtx, "You have visted %d times", count)
	return nil
}

func main() {
	rds, ok := os.LookupEnv("REDIS_ADDRESS")
	if !ok {
		log.Fatalln("REDIS_ADDRESS not set.")
	}

	port, ok := os.LookupEnv("HTTP_PORT")
	if !ok {
		log.Fatalln("HTTP_PORT not set.")
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: rds,
	})

	if _, err := rdb.Ping(context.Background()).Result(); err != nil {
		log.Fatalf("Can't connect to redis at %v", rds)
	}

	app := &App{
		db: rdb,
	}

	g := fastglue.New()

	g.GET("/", app.handleIndex)

	s := &fasthttp.Server{
		Name:         "Redis-Counter",
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	fmt.Printf("Starting server at %s\n", port)
	if err := g.ListenAndServe(port, "", s); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}
