module gitlab.com/saratchandra/redis-counter

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fasthttp/router v1.4.1 // indirect
	github.com/go-redis/redis/v8 v8.11.1
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/valyala/fasthttp v1.28.0
	github.com/zerodha/fastglue v1.6.6
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
