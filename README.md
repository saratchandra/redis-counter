# Redis Counter

For running the binary, you need to install Go.

## Building

- `go mod download`
- `go build`

Run the binary generated: `./redis-counter`

For running the program, these environment variables are needed.

- REDIS_ADDRESS
- HTTP_PORT
